Petit poc pour démontrer la faisabilité de détecter les rimes

* Pour convertir un fichier freedict en fichier json:
    sudo pip3 install pyglossary
    pyglossary /usr/share/dictd/freedict-fra-spa.index dict-fra-spa.json

* Pour convertir ce fichier json en fichier utilisable par checkrime.py
    ./gen-dico-phon.py dict-fra-spa.json dico-phon.json

* Pour vérifier si 2 mots riment:
    ./checkrime.py <mot1> <mot2>

Pour l'instant ça marche à peu près, la plus grosse limitation vient du fait que ça ne supporte pas les noms propres. Il faudrait une autre source pour les ajouter, j'ai peut être une idée. Il y a peut être des cas où ça échoue lamentablement, je ne l'ai pas testé à fond.

La conversion en JS devrait être assez simple, il faudra peut être mettre des séquences d'échappement pour les caratères phonétiques, mais on peut le spécifier dans la fonction Python qui fait ça.
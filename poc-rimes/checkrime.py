#! /usr/bin/python3

import json
import sys

def main():
    if len(sys.argv) != 3:
        print("error : wrong number of parameters")

    word1 = sys.argv[1]
    word2 = sys.argv[2]

    with open('dico-phon.json') as f:
        jj = json.load(f)

    if word1 not in jj.keys():
        print("don't know word " + word1)
        exit(1)

    if word2 not in jj.keys():
        print("don't know word " + word2)
        exit(1)

    if jj[word1][-1:] == jj[word2][-1:]:
        print("ca rime!")
    else:
        print("ca rime pas :(")
            
if __name__ == "__main__":
    main()

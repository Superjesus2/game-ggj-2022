#! /usr/bin/python3

import json
import sys

def main():
    if len(sys.argv) != 3:
        print("error : wrong number of parameters")
        infile = sys.argv[1]
        outfile = sys.argv[2]

        with open(infile) as f:
            jj = json.load(f)

        phon_dict=dict()
        for i in jj.values():
            line=i.split('/')
            if (len(line) >= 2):
                phon_dict[line[0].strip()] = line[1].strip()

            
        with open(outfile, 'w') as f:
            json.dump(phon_dict,f, ensure_ascii=False,indent=4)
     
     
if __name__ == "__main__":
    main()
